// class Product {
//   name;
//   code;
//   price;
//   quantity;
//   barcode;

//   constructor(name, code, price, quantity, barcode) {
//     this.name = name;
//     this.code = code;
//     this.price = price;
//     this.quantity = quantity;
//     this.barcode = barcode;
//   }
// }

// class Document {
//   serie = "";
//   correlative = 0;
//   typeDocument = "";
//   amount = 0;
//   products = [];
//   ticket = "";
//   constructor(params) {
//     const {serie, correlative, typeDocument, products} = params;
//     this.serie = serie;
//     this.correlative = correlative;
//     this.typeDocument = typeDocument;
//     this.products = products;
//     this.amount = this.getAmount();
//     this.ticket = this.getTicket();
//   }
//   getAmount() {
//     let amount = 0;
//     this.products.forEach(product => {
//       amount = amount + product.price * product.quantity;
//     });
//     return amount;
//   }
//   getTicket() {
//     return `${this.serie}-${this.correlative}`;
//   }
// }

// let product1 = new Product("Leche", "0202002", 100, 20, "03394543");
// let product2 = new Product("Arroz", "747474", 1, 5, "54365");

// let products = [];
// products.push(product1);

// let document1 = {
//   products,
//   correlative: 1,
//   typeDocument: "01",
//   serie: "B001"
// };

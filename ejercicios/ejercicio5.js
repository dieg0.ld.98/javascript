let data = {};

// GET, POST, PATH, DELETE
// CRUD

let callApi = async () => {
  const response = await fetch("https://rickandmortyapi.com/api/character", {
      method: "GET",
    })
    .then((response) => {
      return response.json();
    })
    .then((responseJson) => {
      return responseJson;
    })
    .catch((error) => {
      console.log(error);
    });

  console.log("response", response);
  const arrayImages = [];
  let arrayEpisodes = [];
  response.results.forEach((element) => {
    arrayEpisodes = [...arrayEpisodes, ...element.episode];
    arrayImages.push(element.image);
  });

  console.log(search(response.results, ['name', 'species'], "Human"))
};

callApi();

// crear una funciones  cons dos argumentos
// 1er argumento array, 2do argumento un array de keys para la busqueda, 3er argumento, el texto a buscar
// verificar que el key sea un string 

const search = (array = [], searchKeys, text = "") => {
  return array.filter((element) => {
    let search = false;
    searchKeys.forEach((key) => {
      let textSearchUpper = text.toUpperCase()
      let textUpper = `${typeof element[key] === 'string' ? element[key] : ""}`.toUpperCase()
      if (textUpper.indexOf(textSearchUpper) !== -1) {
        search = true;
      }
    })
    return search

  })
}

// const apiX =()=>{

//   funcX (()=>{
//     const f1 =""
//     funcY(()=>{
//       f1
//       funZ (()=>{
//         fun
//       })
//     })

//   })
// }

// const apiX = async ()=>{

//   const f1 = await funcX ()
//   const f2 = await funcY (f1)
//   const f3 = await funZ (f2)

// }
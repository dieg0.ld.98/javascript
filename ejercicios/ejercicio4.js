// ciclos

// let yes = 0;
// let no = 0;
// options.forEach(element => {
//   if (element.status) {
//     yes++;
//   } else {
//     no++;
//   }
// });

// 1: yes, 2:no, 3: na
const data = [
  {
    name: "I. COMPROMISO E INVOLUCRAMIENTO",
    items: [
      {
        name: "PRINCIPIOS",
        options: [
          {
            name:
              "El empleador proporciona los recursos necesarios para que se implemente un sistema de gestión de seguridad y salud en el trabajo.",
            source: "",
            observation: "",
            status: false
          },
          {
            name:
              "Se ha cumplido lo planificado en los diferentes programas de seguridad y salud en el trabajo.",
            source: "",
            observation: "",
            status: true
          },
          {
            name:
              "Se implementan acciones preventivas de seguridad y salud en el trabajo para asegurar la mejora continua.",
            source: "",
            observation: "",
            status: false
          },
          {
            name:
              "Se implementan acciones preventivas de seguridad y salud en el trabajo para asegurar la mejora continua.				",
            source: "",
            observation: "",
            status: true
          },
          {
            name:
              "Se reconoce el desempeño del trabajador para mejorar la autoestima y se fomenta el trabajo en equipo.",
            source: "",
            observation: "",
            status: true
          },
          {
            name:
              "Se realizan actividades para fomentar una cultura de prevención de riesgos del trabajo en toda la empresa, entidad pública o privada.",
            source: "",
            observation: "",
            status: true
          },
          {
            name:
              "Se promueve un buen clima laboral para reforzar la empatía entre empleador, trabajador y viceversa.",
            source: "",
            observation: "",
            status: true
          },
          {
            name:
              "Existen medios que permiten el aporte de los trabajadores al empleador en materia de seguridad y salud en el trabajo.",
            source: "",
            observation: "",
            status: true
          },
          {
            name:
              "Existen mecanismos de reconocimiento del personal proactivo interesado en el mejoramiento continuo de la seguridad y salud en el trabajo.",
            source: "",
            observation: "",
            status: true
          },
          {
            name:
              "Se tiene evaluado los principales riesgos que ocasionan mayores pérdidas.",
            source: "",
            observation: "",
            status: true
          },
          {
            name:
              "Se fomenta la participación de los representantes de trabajadores y de las organizaciones sindicales en las decisiones sobre la seguridad y salud en el trabajo.",
            source: "",
            observation: "",
            status: true
          }
        ]
      }
    ]
  },
  {
    name: "II. POLÍTICA DE SEGURIDAD Y SALUD OCUPACIONAL",
    items: [
      {
        name: "POLÍTICA",
        options: [
          {
            name:
              "Existe una política documentada en materia de seguridad y salud en el trabajo, específica y apropiada para la empresa, entidad pública o privada.",
            source: "",
            observation: "",
            status: false
          },
          {
            name:
              "La política de seguridad y salud en el trabajo está firmada por la máxima autoridad de la empresa, entidad pública o privada.",
            source: "",
            observation: "",
            status: true
          },
          {
            name:
              "Los trabajadores conocen y están comprometidos con lo establecido en la política de seguridad y salud en el trabajo.",
            source: "",
            observation: "",
            status: true
          },
          {
            name: `Su contenido comprende:${"\n"}* El compromiso de protección de todos los miembros de la organización.${"\n"}* Cumplimiento de la normatividad.${"\n"}* Garantía de protección, participación, consulta y participación en los elementos del sistema de gestión de seguridad y salud en el trabajo por parte de los trabajadores y sus representantes.${"\n"}* La mejora continua en materia de seguridad y salud en el trabajo. ${"\n"}* Integración del Sistema de Gestión de Seguridad y Salud en el Trabajo con otros sistemas de ser el caso.`,
            source: "",
            observation: "",
            status: false
          }
        ]
      },
      {
        name: "DIRECCIÓN",
        options: [
          {
            name:
              "Se toman decisiones en base al  análisis de inspecciones,  auditorías, informes de investigación de accidentes, informe de estadísticas, avances de programas de seguridad y salud en el trabajo y opiniones de trabajadores, dando el seguimiento de las mismas.",
            source: "",
            observation: "",
            status: true
          },
          {
            name:
              "El empleador delega funciones y autoridad al personal encargado de implementar el sistema de gestión de Seguridad y Salud en el Trabajo.",
            source: "",
            observation: "",
            status: true
          }
        ]
      },
      {
        name: "LIDERAZGO",
        options: [
          {
            name:
              "El empleador asume el liderazgo  en la gestión de la Seguridad y Salud en el Trabajo.",
            source: "",
            observation: "",
            status: true
          },
          {
            name:
              "El empleador dispone los recursos necesarios para mejorar la gestión de la Seguridad y Salud en el Trabajo.",
            source: "",
            observation: "",
            status: true
          }
        ]
      },
      {
        name: "ORGANIZACIÓN",
        options: [
          {
            name:
              "Existen responsabilidades específicas en seguridad y salud en el trabajo de los niveles de mando de la empresa, entidad pública o privada.",
            source: "",
            observation: "",
            status: true
          },
          {
            name:
              "Se ha destinado presupuesto para implementar o mejorar el sistema de gestión de seguridad y salud el trabajo.",
            source: "",
            observation: "",
            status: true
          },
          {
            name:
              "El Comité o Supervisor de Seguridad y Salud en el Trabajo participa en la definición de estímulos y sanciones.",
            source: "",
            observation: "",
            status: true
          }
        ]
      },
      {
        name: "COMPETENCIA",
        options: [
          {
            name:
              "El empleador ha definido los requisitos de competencia necesarios para cada puesto de trabajo y adopta disposiciones de capacitación en materia de seguridad y salud en el trabajo para que éste asuma sus deberes con responsabilidad.",
            source: "",
            observation: "",
            status: true
          }
        ]
      }
    ]
  },
  {
    name: "III. PLANEAMIENTO Y APLICACIÓN",
    items: [
      {
        name: "DIAGNÓSTICO",
        options: [
          {
            name:
              "Se  ha  realizado  una  evaluación  inicial  o  estudio  de  línea  base  como diagnóstico participativo del estado de la salud y seguridad en el trabajo.",
            source: "",
            observation: "",
            status: false
          },
          {
            name:
              "Los resultados han sido comparados con lo establecido en la Ley de SST y su Reglamento y otros dispositivos legales pertinentes, y servirán de base para planificar, aplicar el sistema y como referencia para medir su mejora continua.",
            source: "",
            observation: "",
            status: true
          },
          {
            name: `La planificación permite: ${"\n"}* Cumplir con normas nacionales ${"\n"}* Mejorar el desempeño. ${"\n"}* Mantener procesos productivos seguros o de servicios seguros.`,
            source: "",
            observation: "",
            status: true
          }
        ]
      },
      {
        name: "DIAGNÓSTICO",
        options: [
          {
            name:
              "Se  ha  realizado  una  evaluación  inicial  o  estudio  de  línea  base  como diagnóstico participativo del estado de la salud y seguridad en el trabajo.",
            source: "",
            observation: "",
            status: false
          },
          {
            name:
              "Los resultados han sido comparados con lo establecido en la Ley de SST y su Reglamento y otros dispositivos legales pertinentes, y servirán de base para planificar, aplicar el sistema y como referencia para medir su mejora continua.",
            source: "",
            observation: "",
            status: true
          },
          {
            name: `La planificación permite: ${"\n"}* Cumplir con normas nacionales ${"\n"}* Mejorar el desempeño. ${"\n"}* Mantener procesos productivos seguros o de servicios seguros.`,
            source: "",
            observation: "",
            status: true
          }
        ]
      },
      {
        name:
          "PLANEAMIENTO PARA LA IDENTIFICACIÓN DE PELIGROS, EVALUACIÓN Y CONTROL DE RIESGOS",
        options: [
          {
            name:
              "El empleador ha establecido procedimientos para identificar peligros y evaluar riesgos.				",
            source: "",
            observation: "",
            status: true
          },
          {
            name: `Comprende estos procedimientos: ${"\n"}* Todas las actividades. ${"\n"}* Todo el personal. ${"\n"}* Todas las instalaciones.`,
            source: "",
            observation: "",
            status: true
          },
          {
            name: `El empleador aplica medidas para:${"\n"}* Gestionar, eliminar y controlar riesgos.${"\n"}* Diseñar ambiente y puesto de trabajo, seleccionar equipos y métodos de trabajo que garanticen la seguridad y salud del trabajador.${"\n"}* Eliminar las situaciones y agentes peligrosos o sustituirlos.${"\n"}* Modernizar los planes y programas de prevención de riesgos laborales${"\n"}* Mantener políticas de protección.${"\n"}* Capacitar anticipadamente al trabajador.`,
            source: "",
            observation: "",
            status: true
          },
          {
            name:
              "El empleador actualiza la evaluación de riesgo una  (01) vez al año como mínimo o cuando cambien las condiciones o se hayan producido daños.",
            source: "",
            observation: "",
            status: true
          },
          {
            name: `La evaluación de riesgo considera: ${"\n"}* Controles periódicos de las condiciones de trabajo y de la salud de los trabajadores. ${"\n"}* Medidas de prevención.`,
            source: "",
            observation: "",
            status: true
          },
          {
            name:
              "Los representantes de los trabajadores han participado en la identificación de peligros y evaluación de riesgos, han sugerido las medidas de control y verificado su aplicación.",
            source: "",
            observation: "",
            status: true
          }
        ]
      },
      {
        name: "OBJETIVOS",
        options: [
          {
            name: `Los objetivos se centran en el logro de resultados realistas y visibles de aplicar, que comprende:  ${"\n"}* Reducción de los riesgos del trabajo. ${"\n"}* Reducción de los accidentes de trabajo y enfermedades ocupacionales. ${"\n"}* La  mejora continua  de  los  procesos, la  gestión del  cambio, la preparación y respuesta a situaciones de emergencia. ${"\n"}* Definición de metas, indicadores, responsabilidades. ${"\n"}* Selección de criterios de medición para confirmar su logro.`,
            source: "",
            observation: "",
            status: true
          },
          {
            name:
              "La empresa, entidad pública o privada cuenta con objetivos cuantificables de seguridad y salud en el trabajo que abarca a todos los niveles de la organización y están documentados.",
            source: "",
            observation: "",
            status: true
          }
        ]
      },
      {
        name: "PROGRAMA DE SEGURIDAD Y SALUD EN EL TRABAJO",
        options: [
          {
            name:
              "Existe un programa anual de Seguridad y Salud en el Trabajo.",
            source: "",
            observation: "",
            status: true
          },
          {
            name:
              "Las  actividades  programadas  están  relacionadas con  el logro de los objetivos.",
            source: "",
            observation: "",
            status: true
          },
          {
            name:
              "Se definen responsables de las actividades en el programa de Seguridad y Salud en el Trabajo.				",
            source: "",
            observation: "",
            status: true
          },
          {
            name:
              "Se definen tiempos y plazos para el cumplimiento y se realiza seguimiento  periódico.",
            source: "",
            observation: "",
            status: true
          },
          {
            name: "Se señala dotación de recursos humanos y económicos.",
            source: "",
            observation: "",
            status: true
          },
          {
            name:
              "Se establecen actividades preventivas ante los riesgos que inciden en la función de procreación del trabajador.",
            source: "",
            observation: "",
            status: true
          }
        ]
      }
    ]
  },
  {
    name: "IV. IMPLEMENTACIÓN Y OPERACIÓN",
    items: [
      {
        name: "ESTRUCTURA Y RESPONSABILIDADES",
        options: [
          {
            name:
              "El Comité de Seguridad y Salud en el Trabajo está constituido de forma paritaria (Para el caso de empleadores con 20 o más trabajadores).",
            source: "",
            observation: "",
            status: true
          },
          {
            name:
              "Existe al menos un Supervisor de Seguridad y Salud (Para el caso de empleadores con menos de 20 trabajadores).",
            source: "",
            observation: "",
            status: true
          },
          {
            name: `El empleador es responsable de: ${"\n"}* Garantizar la seguridad y salud de los trabajadores. ${"\n"}* Actúa para mejorar el nivel de seguridad y salud en el trabajo.  ${"\n"}* Actúa en tomar medidas de prevención de riesgo ante modificaciones de las condiciones de trabajo. ${"\n"}* Realiza los exámenes médicos ocupacionales al trabajador antes, durante y al término de la relación laboral.`,
            source: "",
            observation: "",
            status: true
          },
          {
            name:
              "El empleador considera las competencias del trabajador en materia de seguridad y salud en el trabajo, al asignarle sus labores.",
            source: "",
            observation: "",
            status: true
          },
          {
            name:
              "El empleador controla que solo el personal capacitado y protegido acceda a zonas de alto riesgo.",
            source: "",
            observation: "",
            status: true
          },
          {
            name:
              "El empleador prevé que la exposición a agentes físicos, químicos, biológicos, disergonómicos y psicosociales no generen daño al trabajador o trabajadora.",
            source: "",
            observation: "",
            status: true
          },
          {
            name:
              "El empleador asume los costos de las  acciones de seguridad y salud ejecutadas en el centro de trabajo.",
            source: "",
            observation: "",
            status: true
          }
        ]
      },
      {
        name: "CAPACITACIÓN",
        options: [
          {
            name:
              "El empleador toma medidas para transmitir al trabajador información sobre los riesgos en el centrode trabajo y las medidas de  protección que corresponda.",
            source: "",
            observation: "",
            status: true
          },
          {
            name:
              "El empleador imparte la capacitación dentro de la jornada de trabajo.",
            source: "",
            observation: "",
            status: true
          },
          {
            name:
              "El costo de las capacitaciones es íntegramente asumido por el empleador.",
            source: "",
            observation: "",
            status: true
          },
          {
            name:
              "Los representantes de los trabajadores han revisado el programa de capacitación.",
            source: "",
            observation: "",
            status: true
          },
          {
            name:
              "La capacitación se imparte por personal competente y con experiencia en la materia.",
            source: "",
            observation: "",
            status: true
          },
          {
            name:
              "Se ha capacitado a los integrantes del comité de seguridad y salud en el trabajo o al supervisor de Seguridad y Salud en el Trabajo.",
            source: "",
            observation: "",
            status: true
          },
          {
            name: "Las capacitaciones están documentadas.",
            source: "",
            observation: "",
            status: true
          },
          {
            name: `Se han realizado capacitaciones de Seguridad y Salud en el Trabajo: ${"\n"}* Al momento de la contratación, cualquiera sea la modalidad o duración. ${"\n"}* Durante el desempeño de la labor. ${"\n"}* Específica en el puesto de trabajo o en la función que cada trabajador desempeña, cualquiera que sea la naturaleza del vínculo, modalidad o duración de su contrato. ${"\n"}* Cuando se produce cambios en las funciones que  desempeña el trabajador. ${"\n"}* Cuando se produce cambios en las tecnologías o en los equipos de trabajo. ${"\n"}* En las medidas que permitan la adaptación a la evolución de los riesgos y la prevención de nuevos riesgos. ${"\n"}* Para la actualización periódica de los conocimientos. ${"\n"}* Utilización y mantenimiento preventivo de las maquinarias y equipos. ${"\n"}* Uso apropiado de los materiales peligrosos.`,
            source: "",
            observation: "",
            status: true
          }
        ]
      },
      {
        name: "MEDIDAS DE PREVENCIÓN",
        options: [
          {
            name: `Las medidas de prevención y  protección  se  aplican  en el orden de prioridad: ${"\n"}* Eliminación de los peligros y riesgos. ${"\n"}* Tratamiento, control o aislamiento de los peligros y riesgos, adoptando medidas técnicas o administrativas. ${"\n"}* Minimizar los peligros y riesgos, adoptando sistemas de trabajo seguro que incluyan disposiciones administrativas de control. ${"\n"}* Programar la sustitución progresiva y en la brevedad posible, de los procedimientos, técnicas, medios, sustancias y productos peligrosos por aquellos que produzcan un menor riesgo o ningún riesgo para el trabajador. ${"\n"}*  En último caso, facilitar equipos de protección personal adecuados, asegurándose que los trabajadores los utilicen y conserven en forma correcta.`,
            source: "",
            observation: "",
            status: true
          }
        ]
      },
      {
        name: "PREPARACIÓN Y RESPUESTA ANTE EMERGENCIAS",
        options: [
          {
            name:
              "La empresa, entidad pública o privada ha elaborado planes y procedimientos para enfrentar y responder ante situaciones de emergencias.",
            source: "",
            observation: "",
            status: true
          },
          {
            name:
              "Se tiene organizada la brigada para actuar en caso de: incendios, primeros auxilios, evacuación.",
            source: "",
            observation: "",
            status: true
          },
          {
            name:
              "La empresa, entidad pública o privada revisa los planes y procedimientos ante situaciones de emergencias en forma periódica.",
            source: "",
            observation: "",
            status: true
          },
          {
            name:
              "El empleador ha dado las instrucciones a los trabajadores para que en caso de un peligro grave e inminente puedan interrumpir sus labores y/o evacuar la zona de riesgo.",
            source: "",
            observation: "",
            status: true
          }
        ]
      },
      {
        name:
          "CONTRATISTAS, SUBCONTRATISTAS, EMPRESA, ENTIDAD PÚBLICA O PRIVADA, DE SERVICIOS Y COOPERATIVAS",
        options: [
          {
            name: `Los trabajadores han participado en: ${"\n"}* La consulta, información y capacitación en seguridad y salud en el trabajo. ${"\n"}* La elección de sus representantes ante el Comité de seguridad y salud en el trabajo. ${"\n"}* La conformación del Comité de seguridad y salud en el trabajo. ${"\n"}* El reconocimiento de sus representantes por parte del empleador.`,
            source: "",
            observation: "",
            status: true
          },
          {
            name:
              "Los trabajadores han sido consultados ante los cambios realizados en las operaciones, procesos y organización del trabajo que repercuta en su seguridad y salud.",
            source: "",
            observation: "",
            status: true
          },
          {
            name:
              "Existe procedimientos para asegurar que las informaciones  pertinentes lleguen a los trabajadores correspondientes de la organización.",
            source: "",
            observation: "",
            status: true
          }
        ]
      }
    ]
  },
  {
    name: "V. EVALUACIÓN NORMATIVA",
    items: [
      {
        name: "REQUISITOS LEGALES Y DE OTRO TIPO",
        options: [
          {
            name:
              "La empresa, entidad pública o privada tiene un procedimiento para identificar, acceder y monitorear el cumplimiento de la normatividad aplicable al sistema de gestión de seguridad y salud en el trabajo y se mantiene actualizada.",
            source: "",
            observation: "",
            status: true
          },
          {
            name:
              "La empresa, entidad pública o  privada con 20 o más trabajadores ha elaborado su Reglamento Interno de Seguridad y Salud en el Trabajo.",
            source: "",
            observation: "",
            status: true
          },
          {
            name:
              "La empresa, entidad pública o privada con 20 o más trabajadores tiene un Libro del Comité de Seguridad y Salud en el Trabajo (Salvo que una norma sectorial no establezca un número mínimo inferior)",
            source: "",
            observation: "",
            status: true
          },
          {
            name:
              "Los equipos a presión que posee la empresa entidad pública o privada tienen su libro de servicio autorizado por el MTPE.",
            source: "",
            observation: "",
            status: true
          },
          {
            name:
              "El empleador adopta las medidas necesarias y oportunas, cuando detecta que la utilización de ropas y/o equipos de trabajo o de protección personal representan riesgos específicos para la seguridad y salud de los trabajadores.",
            source: "",
            observation: "",
            status: true
          },
          {
            name:
              "El empleador toma medidas que eviten las labores peligrosas a trabajadoras en periodo de embarazo o lactancia conforme a ley.",
            source: "",
            observation: "",
            status: true
          },
          {
            name:
              "El empleador no emplea a niños, ni adolescentes en actividades peligrosas.",
            source: "",
            observation: "",
            status: true
          },
          {
            name:
              "El empleador evalúa el puesto de trabajo que va a desempeñar un adolescente trabajador previamente a su incorporación laboral a fin de determinar la naturaleza, el grado y la duración de la exposición al riesgo, con el objeto de adoptar medidas preventivas necesarias.",
            source: "",
            observation: "",
            status: true
          },
          {
            name: `La empresa, entidad pública o privada dispondrá lo necesario para que: ${"\n"}* Las máquinas, equipos, sustancias, productos o útiles de trabajo no constituyan una fuente de peligro. ${"\n"}* Se  proporcione  información  y  capacitación  sobre  la   instalación, adecuada utilización y mantenimiento preventivo de las maquinarias y equipos. ${"\n"}* Se proporcione información y capacitación para el uso apropiado de los materiales peligrosos. ${"\n"}* Las instrucciones, manuales, avisos de peligro u otras medidas de precaución colocadas en los equipos y maquinarias estén traducido al castellano. ${"\n"}* Las  informaciones  relativas a las  máquinas, equipos, productos, sustancias o útiles de trabajo son comprensibles para los trabajadores.`,
            source: "",
            observation: "",
            status: true
          },
          {
            name: `Los trabajadores cumplen con: ${"\n"}* Las normas, reglamentos  e  instrucciones  de  los  programas  de seguridad y salud en el trabajo que se apliquen en el lugar de trabajo y con las instrucciones que les impartan sus superiores jerárquicos directos. ${"\n"}* Usar adecuadamente  los  instrumentos  y  materiales  de  trabajo,  así como los equipos de protección personal y colectiva. ${"\n"}* No operar o manipular equipos, maquinarias, herramientas u otros elementos para los cuales no hayan sido autorizados y, en caso de ser necesario, capacitados. ${"\n"}* Cooperar y participar en el proceso de investigación de los accidentes de trabajo, incidentes peligrosos, otros incidentes y las enfermedades ocupacionales cuando la autoridad competente lo requiera. ${"\n"}* Velar por el cuidado integral individual y colectivo, de su salud física y mental. ${"\n"}* Someterse a exámenes médicos obligatorios.* Participar  en  los  organismos  paritarios  de  seguridad  y salud  en  el trabajo. ${"\n"}* Comunicar  al  empleador  situaciones  que  ponga  o  pueda  poner  en riesgo su seguridad y salud y/o las instalaciones físicas. ${"\n"}* Reportar a los representantes de seguridad de forma inmediata,  la ocurrencia de cualquier accidente de trabajo, incidente peligroso o incidente. ${"\n"}* Concurrir a la capacitación y entrenamiento sobre seguridad y salud en el trabajo.`,
            source: "",
            observation: "",
            status: true
          }
        ]
      }
    ]
  },
  {
    name: "VI. VERIFICACIÓN",
    items: [
      {
        name: "SUPERVISIÓN, MONITOREO Y SEGUIMIENTO DE DESEMPEÑO",
        options: [
          {
            name:
              "La vigilancia y control de la seguridad y salud en el trabajo permite evaluar con regularidad los resultados logrados en materia de seguridad y salud en el trabajo.",
            source: "",
            observation: "",
            status: true
          },
          {
            name: `La supervisión permite: ${"\n"}* Identificar las fallas o deficiencias en el sistema de gestión de la seguridad y salud en el trabajo. ${"\n"}* Adoptar las medidas preventivas y correctivas.`,
            source: "",
            observation: "",
            status: true
          },
          {
            name:
              "  El monitoreo permite la medición cuantitativa y cualitativa apropiadas.",
            source: "",
            observation: "",
            status: true
          },
          {
            name:
              "Se monitorea el grado de cumplimiento de los objetivos de la seguridad y salud en el trabajo.",
            source: "",
            observation: "",
            status: true
          }
        ]
      },
      {
        name: "SALUD EN EL TRABAJO",
        options: [
          {
            name:
              "El empleador realiza exámenes médicos antes, durante y al término de la relación laboral a los trabajadores (incluyendo a los adolescentes).",
            source: "",
            observation: "",
            status: true
          },
          {
            name: `Los trabajadores son informados: ${"\n"}* A título grupal, de las razones para los exámenes de salud ocupacional. ${"\n"}* A título personal, sobre los resultados de los informes médicos relativos a la evaluación de su salud. ${"\n"}* Los resultados de los exámenes médicos no son pasibles de uso para ejercer discriminación.`,
            source: "",
            observation: "",
            status: true
          },
          {
            name:
              "Los resultados de los exámenes médicos son considerados para tomar acciones preventivas o correctivas al respecto.",
            source: "",
            observation: "",
            status: true
          }
        ]
      },
      {
        name:
          "ACCIDENTES, INCIDENTES PELIGROSOS E INCIDENTES, NO CONFORMIDAD, ACCIÓN CORRECTIVA Y PREVENTIVA",
        options: [
          {
            name:
              "El empleador notifica al Ministerio de Trabajo y Promoción del Empleo los accidentes de trabajo mortales dentro de las 24 horas de ocurridos.",
            source: "",
            observation: "",
            status: true
          },
          {
            name:
              "El empleador notifica al Ministerio de Trabajo y Promoción del Empleo, dentro de las 24 horas de producidos, los incidentes peligrosos que han puesto en riesgo la salud y la integridad física de los trabajadores y/o a la población.",
            source: "",
            observation: "",
            status: true
          },
          {
            name:
              "Se implementan las medidas correctivas propuestas en los registros de accidentes de trabajo, incidentes peligrosos y otros incidentes.",
            source: "",
            observation: "",
            status: true
          },
          {
            name:
              "Se implementan las medidas correctivas producto de la no conformidad hallada en las auditorías de seguridad y salud en el trabajo.",
            source: "",
            observation: "",
            status: true
          },
          {
            name:
              "Se implementan medidas preventivas de seguridad y salud en el trabajo.",
            source: "",
            observation: "",
            status: true
          }
        ]
      },
      {
        name: "INVESTIGACIÓN DE ACCIDENTES Y ENFERMEDADES OCUPACIONALES",
        options: [
          {
            name:
              "El empleador ha realizado las investigaciones de accidentes de trabajo, enfermedades ocupacionales e incidentes peligrosos, y ha comunicado a la autoridad administrativa de trabajo, indicando las medidas correctivas  y preventivas adoptadas.",
            source: "",
            observation: "",
            status: true
          },
          {
            name: `Se investiga  los accidentes  de  trabajo,  enfermedades  ocupacionales  e incidentes peligrosos para: ${"\n"}* Determinar las causas e implementar las medidas correctivas. ${"\n"}* Comprobar la eficacia de las medidas de seguridad y salud vigentes al momento de hecho. ${"\n"}* Determinar la  necesidad modificar dichas medidas.`,
            source: "",
            observation: "",
            status: true
          },
          {
            name:
              "Se toma medidas correctivas para reducir las  consecuencias de accidentes.",
            source: "",
            observation: "",
            status: true
          },
          {
            name:
              "Se ha documentado los cambios en los procedimientos como consecuencia de las acciones correctivas.",
            source: "",
            observation: "",
            status: true
          },
          {
            name:
              "El trabajador ha sido transferido en caso de accidente de trabajo o enfermedad ocupacional a otro puesto que implique menos riesgo.",
            source: "",
            observation: "",
            status: true
          }
        ]
      },
      {
        name: "CONTROL DE LAS OPERACIONES",
        options: [
          {
            name:
              "La empresa, entidad pública o privada ha identificado las operaciones y actividades que están asociadas con riesgos donde las medidas de control necesitan ser aplicadas.",
            source: "",
            observation: "",
            status: true
          },
          {
            name:
              "La empresa, entidad pública o privada ha establecido procedimientos para el diseño del lugar de trabajo, procesos operativos, instalaciones, maquinarias  y organización del trabajo que incluye la adaptación a las capacidades humanas a modo de reducir los riesgos en sus fuentes.",
            source: "",
            observation: "",
            status: true
          }
        ]
      },
      {
        name: "GESTIÓN DEL CAMBIO",
        options: [
          {
            name:
              "Se ha evaluado las medidas de seguridad debido  a cambios  internos, método de trabajo, estructura organizativa y cambios externos normativos, conocimientos en el campo de la seguridad, cambios tecnológicos, adaptándose las medidas de prevención antes de introducirlos.",
            source: "",
            observation: "",
            status: true
          }
        ]
      },
      {
        name: "AUDITORÍAS",
        options: [
          {
            name: "Se cuenta con un programa de auditorías.",
            source: "",
            observation: "",
            status: true
          },
          {
            name:
              "El empleador realiza auditorías internas periódicas para comprobar la adecuada aplicación del sistema de gestión de la seguridad y salud en el trabajo.",
            source: "",
            observation: "",
            status: true
          },
          {
            name:
              "Las auditorías externas son realizadas por auditores independientes con la participación de los trabajadores o sus representantes.",
            source: "",
            observation: "",
            status: true
          },
          {
            name:
              "Los resultados de las auditorías son comunicados a la alta dirección de la empresa, entidad pública o privada.",
            source: "",
            observation: "",
            status: true
          }
        ]
      }
    ]
  },
  {
    name: "VII. CONTROL DE INFORMACIÓN Y DOCUMENTOS",
    items: [
      {
        name: "DOCUMENTOS",
        options: [
          {
            name:
              "La empresa, entidad pública o privada establece y mantiene información en medios apropiados para describir los componentes del sistema de gestión y su relación entre ellos.",
            source: "",
            observation: "",
            status: true
          },
          {
            name:
              "Los procedimientos de la empresa, entidad pública o privada, en la gestión de la seguridad y salud en el trabajo, se revisan periódicamente.",
            source: "",
            observation: "",
            status: true
          },
          {
            name: `El empleador establece y mantiene disposiciones y procedimientos para:${"\n"}* Recibir, documentar y responder adecuadamente a las comunicaciones internas y externas relativas a la seguridad y salud en el trabajo.${"\n"}* Garantizar la comunicación interna de la información relativa a la seguridad y salud en el trabajo entre los distintos niveles y cargos de la organización.${"\n"}* Garantizar que las sugerencias de los trabajadores o de sus representantes sobre seguridad y salud en el trabajo se reciban y atiendan en forma oportuna y adecuada.`,
            source: "",
            observation: "",
            status: true
          },
          {
            name:
              "El empleador entrega adjunto a los contratos de trabajo las recomendaciones de seguridad y salud considerando los riesgos del centro de labores y los relacionados con el puesto o función del trabajador.",
            source: "",
            observation: "",
            status: true
          },
          {
            name: `El empleador ha:${"\n"}* Facilitado al trabajador una copia del reglamento interno de seguridad y salud en el trabajo.${"\n"}* Capacitado al trabajador  en referencia  al contenido del reglamento interno de seguridad.${"\n"}* Asegurado poner en práctica las medidas de seguridad y salud en el trabajo.${"\n"}* Elaborado un mapa de riesgos del centro de trabajo y lo exhibe en un lugar visible.${"\n"}* El empleador entrega al trabajador las recomendaciones de seguridad y salud en el trabajo considerando los riesgos del centro de labores y los relacionados con el puesto o función, el primer día de labores.`,
            source: "",
            observation: "",
            status: true
          },
          {
            name: `El empleador mantiene procedimientos para garantizar que:${"\n"}* Se identifiquen, evalúen e incorporen en las especificaciones relativas a compras y arrendamiento financiero, disposiciones relativas al cumplimiento por parte de la organización de  los requisitos  de seguridad y salud.${"\n"}* Se identifiquen las obligaciones y los requisitos tanto legales como de la propia organización en materia de seguridad y salud en el trabajo antes de la adquisición de bienes y servicios.${"\n"}* Se adopten disposiciones para que se cumplan dichos requisitos antes de utilizar los bienes y servicios mencionados.`,
            source: "",
            observation: "",
            status: true
          }
        ]
      },
      {
        name: "CONTROL DE LA DOCUMENTACIÓN Y DE LOS DATOS",
        options: [
          {
            name:
              "La empresa, entidad pública o privada establece procedimientos para el control de los documentos que se generen por esta lista de verificación.",
            source: "",
            observation: "",
            status: true
          },
          {
            name: `Este control asegura que los documentos y datos:${"\n"}* Puedan ser fácilmente localizados.${"\n"}* Puedan ser analizados y verificados periódicamente.${"\n"}* Están disponibles en los locales.${"\n"}* Sean removidos cuando los datos sean obsoletos.${"\n"}* Sean adecuadamente archivados.`,
            source: "",
            observation: "",
            status: true
          }
        ]
      },
      {
        name: "GESTIÓN DE LOS REGISTROS",
        options: [
          {
            name: `El empleador ha implementado registros y documentos del sistema de gestión actualizados y de disposición del trabajador referido a: ${"\n"}* Registro de accidentes de trabajo, enfermedades ocupacionales, incidentes peligrosos y otros incidentes, en el que deben constar la investigación y las medidas correctivas.`,
            source: "",
            observation: "",
            status: true
          },
          {
            name: `* Registro de exámenes médicos ocupacionales.`,
            source: "",
            observation: "",
            status: true
          },
          {
            name: `* Registro de monitoreo de agentes físicos, químicos, biológicos, psicosociales y factores de riesgos disergonómicos.`,
            source: "",
            observation: "",
            status: true
          },
          {
            name: `* Registro de inspecciones internas de seguridad y salud en el trabajo.`,
            source: "",
            observation: "",
            status: true
          },
          {
            name: `* Registro de estadísticas de seguridad y salud.`,
            source: "",
            observation: "",
            status: true
          },
          {
            name: `* Registro de equipos de seguridad o emergencia.`,
            source: "",
            observation: "",
            status: true
          },
          {
            name: `* Registro de inducción, capacitación, entrenamiento y simulacros de emergencia.`,
            source: "",
            observation: "",
            status: true
          },
          {
            name: `* Registro de auditorías.`,
            source: "",
            observation: "",
            status: true
          },
          {
            name: `La empresa, entidad pública o privada cuenta con registro de accidente de trabajo y enfermedad ocupacional e incidentes peligrosos y otros incidentes ocurridos a:${"\n"}* Sus trabajadores.${"\n"}* Trabajadores de intermediación laboral y/o tercerización.${"\n"}* Beneficiarios bajo modalidades formativas.${"\n"}* Personal que presta servicios de manera independiente, desarrollando sus actividades total o parcialmente en las instalaciones de la empresa, entidad pública o privada.`,
            source: "",
            observation: "",
            status: true
          },
          {
            name: `Los registros mencionados son:${"\n"}* Legibles e identificables.${"\n"}* Permite su seguimiento.${"\n"}* Son archivados y adecuadamente protegidos`,
            source: "",
            observation: "",
            status: true
          }
        ]
      }
    ]
  },
  {
    name: "VIII. REVISIÓN POR LA DIRECCIÓN",
    items: [
      {
        name: "GESTIÓN DE LA MEJORA CONTINUA",
        options: [
          {
            name:
              "La alta dirección: Revisa y analiza periódicamente el sistema de gestión para asegurar que es apropiada y efectiva.",
            source: "",
            observation: "",
            status: true
          },
          {
            name: `Las disposiciones adoptadas por la dirección para la mejora continua del sistema de gestión de la seguridad y salud en el trabajo, deben tener en cuenta: ${"\n"}* Los objetivos de la seguridad y salud en el trabajo de la empresa, entidad pública o privada. ${"\n"}* Los resultados de la identificación de los peligros y evaluación de los riesgos. ${"\n"}* Los resultados de la supervisión y medición de la eficiencia. ${"\n"}* La investigación de accidentes, enfermedades ocupacionales, incidentes peligrosos y otros incidentes relacionados con el trabajo. ${"\n"}* Los resultados y recomendaciones de las auditorías y evaluaciones realizadas por la dirección de la empresa, entidad pública o privada. ${"\n"}* Las  recomendaciones  del  comité de seguridad y salud, del supervisor de seguridad y salud. ${"\n"}* Los cambios en las normas. ${"\n"}* La información pertinente nueva. ${"\n"}* Los resultados de los programas anuales de seguridad y salud en el trabajo.`,
            source: "",
            observation: "",
            status: true
          },
          {
            name: `La metodología de mejoramiento continuo considera: ${"\n"}* La identificación de las desviaciones de las prácticas y condiciones aceptadas como seguras. ${"\n"}* El establecimiento de estándares de seguridad. ${"\n"}* La medición y evaluación periódica del desempeño con respecto a los estándares de la empresa, entidad pública o privada. ${"\n"}* La corrección y reconocimiento del desempeño`,
            source: "",
            observation: "",
            status: true
          },
          {
            name:
              "La investigación y auditorías permiten a la dirección de la empresa, entidad pública o privada lograr los fines previstos y determinar, de ser el caso, cambios en la política y objetivos del sistema de gestión de seguridad y salud en el trabajo.",
            source: "",
            observation: "",
            status: true
          },
          {
            name: `La investigación de los accidentes, enfermedades ocupacionales, incidentes peligrosos y otros incidentes, permite identificar: ${"\n"}* Las causas inmediatas (actos y condiciones subestándares), ${"\n"}* Las causas básicas (factores personales y factores del trabajo) ${"\n"}* Deficiencia del sistema de gestión de la seguridad y salud en el trabajo, para la planificación de la acción correctiva pertinente.`,
            source: "",
            observation: "",
            status: true
          },
          {
            name:
              "El  empleador  ha  modificado  las  medidas  de  prevención  de  riesgos laborales cuando resulten inadecuadas e insuficientes para garantizar la seguridad y salud de los trabajadores incluyendo al personal de los regímenes de intermediación y tercerización, modalidad formativa e incluso a los que prestan servicios de manera independiente, siempre que éstos desarrollen sus actividades total o parcialmente en las instalaciones de la empresa, entidad pública o privada durante el desarrollo de las operaciones.",
            source: "",
            observation: "",
            status: true
          }
        ]
      }
    ]
  }
];

// contar las opciones

const getCountOptions = options => {
  let yes = 0;
  let no = 0;

  options.forEach(option => {
    if (option.status) {
      yes++;
    } else {
      no++;
    }
  });
  return {
    no,
    yes
  };
};

// contar los items
const getCountItems = items => {
  yes = 0;
  no = 0;
  items.forEach(item => {
    // sumando todos los opciones del un item
    const countOptions = getCountOptions(item.options);
    // sumando con los items anteriores
    yes += countOptions.yes;
    no += countOptions.no;
  });
  total_options = yes + no;
  /// (x/100)* total(total_options) = yes
  return {
    yes: {number: no, pecentage: `${Math.round((no * 100) / total_options)}%`},
    no: {number: yes, pecentage: `${Math.round((yes * 100) / total_options)}%`}
  };
};

data.forEach(element => {
  const count = getCountItems(element.items);
  console.log(element.name, count);
});


//
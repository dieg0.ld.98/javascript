// 1- funciones flecha (programacion funcional)
// 2- spread operator
// 3- arrays
// 4- objetos(programacion orientada a objetos)
// 5- desestructuración
// 6- clases -Herencia

// funciones asincronas
// pasar una funcion asincrona a sincrona (async await)
// promesas
// callback
// fetch

// 2
// Math.min(...[1,2,3,4,5])

//3
// let anys = [1, null, undefined, [1, 2, 3], () => {}, "esto", {}, true];

// acceder al elemeto por su indice
// let index =0;
// anys[index]

// saber el ultimo elemento
//anys[anys.length - 1];

// let functionX = (param1) => {
//   console.log(param1);
// };

// functionX();

// desestructuracion
// let anys = ["soy un texto", () => {}, undefined];
// let [text, func, any="-"] = anys

// let names = ["Diego", "Mariano", "Alex"]
// agrega al final
// names.push("Carlos")
// quita al final
// names.pop()

// agrega al inicio
// names.unshift("Carlos");
// quita al inicio
// names.shift();

// .splice(startIndex,quantity,value1,value2,value3...)
// names.splice(1, 0, "Mariano", "Carlos")

// names.slice(0,1)

// let array = ["a", "b", "c"];

// for (let i = 0; i < array.length; i++) {
//   text += array[i];
// }
//

// poner texto al reves
// let name = "Mariano";
// undefined;
// name.split("")(7)[("M", "a", "r", "i", "a", "n", "o")];
// name.split("").sort()(7)[("M", "a", "a", "i", "n", "o", "r")];
// name.split("").reverse()(7)[("o", "n", "a", "i", "r", "a", "M")];
// name
//   .split("")
//   .reverse()
//   .join("");
// ("onairaM");

// unir array
// let names = ["Diego", "Mariano", "Alex"]
// let newNames = ["nName1", "nName2"]
// names.concat(newNames)

// buscar elementos dentro de un array
// let text = "Diego";
// let names = ["Diego", "Mariano", "Alex", "Carlos"];
// names.indexOf("Carlos");

// let arrays = ["Diego", "Mariano", "Alex"];
// let callback = (elemet, index) => {
//   let searchKey = "d".toLowerCase();
//   let elementLowercase = elemet.toLowerCase();
//   if (elementLowercase.indexOf(searchKey) !== -1) {
//     return true;
//   }
// };
// console.log(arrays.find(callback));
// console.log(arrays.findIndex(callback));

// recorrer arrays

// let numbers = [1, 100, 200, 100];
// let text = "Textox";
// for (let i = 0; i < text.length; i++) {
//   console.log(i);
//   console.log(text[i]);
// }

// let users = ["Alex", "Mariano", "Diego"];
// for (const index in users) {
//   console.log(users[index]);
// }

/// forEach // recorrer arreglos
// let count = 0;
// let array10 = [];
// const callback = (element, index) => {
//   if (element === 10) {
//     array10.push(element);
//   }
//   count += element;
// };

// // let users = ["Alex", "Mariano", "Diego"];
// // users.forEach(callback);

// let numbers = [10, 10, 100];
// numbers.forEach(callback);
// console.log(count);

// // some
// let arrays = ["Diego", "Mariano", "Alex"];
// let callback = (elemet, index) => {
//   let searchKey = "d".toLowerCase();
//   let elementLowercase = elemet.toLowerCase();
//   if (elementLowercase.indexOf(searchKey) !== -1) {
//     return true;
//   }
// };
// console.log(arrays.some(callback));

// // every
// // some
// let arrays = ["Diego", "DMariano", "DAlex"];
// let callback = (elemet, index) => {
//   let searchKey = "d".toLowerCase();
//   let elementLowercase = elemet.toLowerCase();
//   if (elementLowercase.indexOf(searchKey) !== -1) {
//     return true;
//   }
// };
// console.log(arrays.some(callback));
// let numbers = [10, 20, 30, 50];
// let callback = (elemet, index) => {
//   if (elemet >= 10) {
//     return true;
//   }
// };
// console.log(numbers.every(callback));

// let sales = [{date: "2020-02-12", document: "B001-100"}];

// map
// let sales = [
//   {serie: "B001", correlative: "1"},
//   {serie: "B002", correlative: "100"}
// ];

// let callback = (element, index) => {
//   // clonando
//   let newSale = {...element};
//   //
//   newSale.nDoc = `${element.serie}-${element.correlative}`;
//   return newSale;
// };

// console.log(sales.map(callback));
// console.log(sales);

//

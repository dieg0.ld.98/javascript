class Humano {
  genero;

  constructor(genero) {
    this.genero = genero;
  }
}

class Usuario extends Humano {
  name;
  lastname;
  age;

  constructor(name, lastname, age, genero) {
    super(genero);
    this.name = name;
    this.lastname = lastname;
    this.age = age;
  }

  getCompleteName() {
    return `${this.name} ${this.lastname}`;
  }

  esMayorDeEdad() {
    if (this.age >= 18) {
      return "Es mayor de edad";
    }
    return "Es menor de edad";
  }
}

let usuario1 = {name: undefined, lastname: undefined, age: undefined};
usuario1.name = "Diego";
console.log(usuario1);

let usuario2 = new Usuario("Diego", "Sanchez", 21);
console.log(usuario2);
console.log(`Nombre completo: ${usuario2.getCompleteName()}`);
console.log(`${usuario2.esMayorDeEdad()}`);

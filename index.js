// // valor (Primitivos)

// let number = 10;
// let numberX = number;
// numberX = 100;
// console.log(number);
// console.log(numberX);

// //referencia (Compuestos)
// let array = [];
// let arrayX = array;
// arrayX.push(1);
// console.log(array);
// console.log(arrayX);

// let text1 = "texto 1"
// {
//   let text1 = "texto 2";

// }

// let producto = {price: 100, costo: 70};
// if (1 > 90 && producto.costo < 90) {
//   console.log("es igual");
//   console.log("es igual");
// }

// let name = "Diego";
// let lastname = null;
// let completeName = `${name} ${lastname ? lastname : "no tiene apellido"}`;
// console.log(completeName);

// if anidado
// let array = "sdffgg";
// if (array) {
//   if (Array.isArray()) {
//     array.push(1);
//     console.log(array);
//   } else {
//     console.log("la variable no es un array");
//   }
// } else {
//   console.log("No Hay información");
// }

// let typeDocument = 1;
// switch (typeDocument) {
//   case 1:
//     console.log("esto es una pre cuenta");
//     break;
//   case 2:
//     console.log("esto es una boleta");
//     break;
//   case 3:
//     console.log("esto es una factura");
//     break;
//   default:
//     console.log("no es ningún tipo");
//     break;
// }

// for
// let text = "Diego";
// text.length;
// for (let i = 0; i < text.length; i++) {
//   if (text[i] === "i") {
//     break;
//   }
//   console.log(text[i]);
// }
